<p>Keyoxide is more than this website. It's a project that aims to make certain cryptographic much more accessible to everyone. Keyoxide is part of a larger community of people working hard to develop tools that add privacy and security to our digital online lives. Remember: privacy is not a luxury.</p>

<p>As Keyoxide is an open-source project licensed under the permissive <a href="https://codeberg.org/yarmo/keyoxide/src/branch/main/LICENSE">MIT License</a>, everyone is welcome and encouraged to contribute. This can be done in various forms:</p>

<ul>
    <li><a href="https://codeberg.org/yarmo/keyoxide/issues">Open an issue</a> to request changes, new features or simply get help.</li>
    <li><a href="https://codeberg.org/yarmo/keyoxide/pulls">Open a PR</a> to directly integrate your own changes and new features.</li>
</ul>

<p>Not a developer? Not a problem? You could:</p>

<ul>
    <li>Learn more about the importance of online privacy and security and advocate for it (much needed!)</li>
    <li>Write guides for others and help each other out.</li>
    <li>Start using decentralized OpenPGP identity keys.</li>
    <li>Spread the word about Keyoxide and OpenPGP keys in general.</li>
    <li>Talk to persons you know using siloed or closed-source alternatives to Keyoxide.</li>
</ul>
